
#include <stdio.h>
#include <stdint.h>
#include "sdkconfig.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "esp_check.h"
#include "esp_check.h"
#include "nvs_flash.h"
#include "esp_wifi.h"

#include "alarm_led.h"
#include "alarm_buttons.h"
#include "zigbee_configuration.h"
#include "sleep_configuration.h"

// TODO:
// Find a nice way to give a device a unique ID after flashing
// We don't want to have to compile new code to give a device a unique ID
#define DEVICE_SERIAL_NUMBER 1
#define DEVICE_UNIQUE_ID DEVICE_SERIAL_NUMBER

static const char *TAG = "main";

void app_init(void)
{
    /* Configure the alarm LED gpio pin */
    configure_alarm_led();

    /* Initialize the Zigbee stack */
    zigbee_configuration();
}

void app_main(void)
{
    // Initialize the application
    app_init();

    // while (1)
    // {
    /* TODO: If the device is in here, the unit has booted due to a button press.
    Check if another button press occurs while waiting for hospital employee to turn off the alarm */

    // }
}