#ifndef ZIGBEE_CONFIGURATION_H
#define ZIGBEE_CONFIGURATION_H

#include "esp_zigbee_core.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_check.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "ha/esp_zigbee_ha_standard.h"
#include "esp_sleep.h"
#include "sleep_configuration.h"

/* Zigbee configuration */
#define INSTALLCODE_POLICY_ENABLE false /* enable the install code policy for security */
#define ED_AGING_TIMEOUT ESP_ZB_ED_AGING_TIMEOUT_64MIN
#define ED_KEEP_ALIVE 3000                     /* 3000 millisecond */
#define ESP_ZB_PRIMARY_CHANNEL_MASK 0x04000000 /* Use only channel 26 */

#define ESP_ALARM_SWITCH_ENDPOINT 10
#define ESP_SERVICE_SWITCH_ENDPOINT 11
#define ESP_ALARM_LED_ENDPOINT 12

#define ESP_ZB_ZED_CONFIG()                               \
    {                                                     \
        .esp_zb_role = ESP_ZB_DEVICE_TYPE_ED,             \
        .install_code_policy = INSTALLCODE_POLICY_ENABLE, \
        .nwk_cfg.zed_cfg = {                              \
            .ed_timeout = ED_AGING_TIMEOUT,               \
            .keep_alive = ED_KEEP_ALIVE,                  \
        },                                                \
    }

#define ESP_ZB_DEFAULT_RADIO_CONFIG()    \
    {                                    \
        .radio_mode = RADIO_MODE_NATIVE, \
    }

#define ESP_ZB_DEFAULT_HOST_CONFIG()                       \
    {                                                      \
        .host_connection_mode = HOST_CONNECTION_MODE_NONE, \
    }

void zigbee_configuration(void);
void zb_button_handler(uint8_t button_endpoint);

#endif // ZIGBEE_CONFIGURATION_H