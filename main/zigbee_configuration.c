/*
 * SPDX-FileCopyrightText: 2021-2024 Espressif Systems (Shanghai) CO LTD
 *
 * SPDX-License-Identifier: LicenseRef-Included
 *
 * Zigbee HA_on_off_light Example
 *
 * This example code is in the Public Domain (or CC0 licensed, at your option.)
 *
 * Unless required by applicable law or agreed to in writing, this
 * software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.
 */

#include "zigbee_configuration.h"
#include "hal/ieee802154_ll.h"
#include "alarm_led.h"
#include "esp_sleep.h"
#include "esp_timer.h"

static const char *TAG = "zigbee_configuration";

static esp_timer_handle_t s_oneshot_timer;
static esp_timer_handle_t send_command_timeout_timer;

/********************* Define functions **************************/
static void bdb_start_top_level_commissioning_cb(uint8_t mode_mask)
{
    ESP_ERROR_CHECK(esp_zb_bdb_start_top_level_commissioning(mode_mask));
}

static void send_command_timeout_timer_init(void)
{
    esp_timer_create_args_t send_command_timeout_timer_args = {
        .callback = &zb_button_handler,
        .name = "send_command_timeout_timer"};

    ESP_ERROR_CHECK(esp_timer_create(&send_command_timeout_timer_args, &send_command_timeout_timer));
}

void zb_button_handler(uint8_t button_endpoint)
{
    static uint8_t prev_button_endpoint = 0;
    esp_zb_zcl_on_off_cmd_t cmd_req;
    if (button_endpoint == 0)
    {
        cmd_req.zcl_basic_cmd.src_endpoint = prev_button_endpoint;
    }
    else
    {
        cmd_req.zcl_basic_cmd.src_endpoint = button_endpoint;
        prev_button_endpoint = button_endpoint;
    }

    cmd_req.zcl_basic_cmd.dst_endpoint = 0x00;
    cmd_req.address_mode = ESP_ZB_APS_ADDR_MODE_DST_ADDR_ENDP_NOT_PRESENT;
    cmd_req.on_off_cmd_id = ESP_ZB_ZCL_CMD_ON_OFF_ON_ID;
    esp_zb_lock_acquire(portMAX_DELAY);
    esp_zb_zcl_on_off_cmd_req(&cmd_req);
    esp_zb_lock_release();
    ESP_EARLY_LOGI(TAG, "Send 'turn on' command to endpoint %d", prev_button_endpoint);

    esp_timer_start_once(send_command_timeout_timer, 1000000); // 1 second before command is re-sent
}

static void s_oneshot_timer_callback(void *arg)
{
    ESP_LOGI(TAG, "Entering deep sleep");
    esp_deep_sleep_start();
}

static void zb_deep_sleep_init(void)
{
    /* Setup a one-shot timer that will start when the device turns to the CHILD state for the first time
    After a 5-second delay, the device will enter deep sleep */

    const esp_timer_create_args_t s_one_shot_timer_args = {
        .callback = &s_oneshot_timer_callback,
        .name = "one_shot_timer",
    };

    ESP_ERROR_CHECK(esp_timer_create(&s_one_shot_timer_args, &s_oneshot_timer));

    // Print the wakeup reason
    handle_wakeup_cause();

    /* Set the methods of how to wake up */
    /* 1. RTC timer */
    // const int wakeup_time_usec = TIMER_WAKEUP_DURATION_US;
    // ESP_LOGI(TAG, "Enabling timer wakeup, %ds", wakeup_time_usec / 1000000);
    // ESP_ERROR_CHECK(esp_sleep_enable_timer_wakeup(wakeup_time_usec));

    /* 2. GPIO */
    static const uint16_t gpio_wakeup_pin_mask = ALARM_BUTTON_GPIO_MASK | SERVICE_BUTTON_LEFT_GPIO_MASK | SERVICE_BUTTON_RIGHT_GPIO_MASK;
    ESP_LOGI(TAG, "Enabling GPIO wakeup, GPIO mask: 0x%x", gpio_wakeup_pin_mask);
    configure_deep_sleep_gpio_pins();
    // ESP_ERROR_CHECK(esp_sleep_enable_ext1_wakeup(gpio_wakeup_pin_mask, ESP_EXT1_WAKEUP_ANY_HIGH));
}

static void zb_deep_sleep_start(void)
{
    /* Start the one-shot timer */
    const int before_deep_sleep_time_sec = 2;
    ESP_LOGI(TAG, "Starting one-shot timer for %ds to enter the deep sleep", before_deep_sleep_time_sec);
    ESP_ERROR_CHECK(esp_timer_start_once(s_oneshot_timer, before_deep_sleep_time_sec * 1000000));
}

void esp_zb_app_signal_handler(esp_zb_app_signal_t *signal_struct)
{
    uint32_t *p_sg_p = signal_struct->p_app_signal;
    esp_err_t err_status = signal_struct->esp_err_status;
    esp_zb_app_signal_type_t sig_type = *p_sg_p;
    switch (sig_type)
    {
    case ESP_ZB_ZDO_SIGNAL_SKIP_STARTUP:
        ESP_LOGI(TAG, "Zigbee stack initialized");
        esp_zb_bdb_start_top_level_commissioning(ESP_ZB_BDB_MODE_INITIALIZATION);
        break;
    case ESP_ZB_BDB_SIGNAL_DEVICE_FIRST_START:
    case ESP_ZB_BDB_SIGNAL_DEVICE_REBOOT:
        if (err_status == ESP_OK)
        {
            ESP_LOGI(TAG, "Device started up in %s factory-reset mode", esp_zb_bdb_is_factory_new() ? "" : "non");
            if (esp_zb_bdb_is_factory_new())
            {
                ESP_LOGI(TAG, "Start network steering");
                esp_zb_bdb_start_top_level_commissioning(ESP_ZB_BDB_MODE_NETWORK_STEERING);
            }
            else
            {
                ESP_LOGI(TAG, "Device rebooted");

                const uint64_t gpio_wakeup_cause = esp_sleep_get_gpio_wakeup_status();
                if (gpio_wakeup_cause & ALARM_BUTTON_GPIO_MASK)
                {
                    zb_button_handler(ESP_ALARM_SWITCH_ENDPOINT);
                }
                else if (gpio_wakeup_cause & (SERVICE_BUTTON_LEFT_GPIO_MASK | SERVICE_BUTTON_RIGHT_GPIO_MASK))
                {
                    zb_button_handler(ESP_SERVICE_SWITCH_ENDPOINT);
                }
                else
                {
                    ESP_LOGI(TAG, "Not a deep sleep reset");
                    zb_deep_sleep_start();
                }
            }
        }
        else
        {
            /* commissioning failed */
            ESP_LOGW(TAG, "Failed to initialize Zigbee stack (status: %s)", esp_err_to_name(err_status));
            // TODO: Handle error in a good way
            // For now, go back to sleep to allow user to try again
            zb_deep_sleep_start();
        }
        break;
    case ESP_ZB_BDB_SIGNAL_STEERING:
        if (err_status == ESP_OK)
        {
            esp_zb_ieee_addr_t extended_pan_id;
            esp_zb_get_extended_pan_id(extended_pan_id);
            ESP_LOGI(TAG, "Joined network successfully (Extended PAN ID: %02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x, PAN ID: 0x%04hx, Channel:%d, Short Address: 0x%04hx)",
                     extended_pan_id[7], extended_pan_id[6], extended_pan_id[5], extended_pan_id[4],
                     extended_pan_id[3], extended_pan_id[2], extended_pan_id[1], extended_pan_id[0],
                     esp_zb_get_pan_id(), esp_zb_get_current_channel(), esp_zb_get_short_address());
            zb_deep_sleep_start();
        }
        else
        {
            ESP_LOGI(TAG, "Network steering was not successful (status: %s)", esp_err_to_name(err_status));
            esp_zb_scheduler_alarm((esp_zb_callback_t)bdb_start_top_level_commissioning_cb, ESP_ZB_BDB_MODE_NETWORK_STEERING, 1000);
        }
        break;

    case ESP_ZB_COMMON_SIGNAL_CAN_SLEEP:
        ESP_LOGI(TAG, "Can sleep");
        break;
    default:
        ESP_LOGI(TAG, "ZDO signal: %s (0x%x), status: %s", esp_zb_zdo_signal_to_string(sig_type), sig_type,
                 esp_err_to_name(err_status));
        break;
    }
}

static esp_err_t zb_attribute_handler(const esp_zb_zcl_set_attr_value_message_t *message)
{
    esp_err_t ret = ESP_OK;

    ESP_RETURN_ON_FALSE(message, ESP_FAIL, TAG, "Empty message");
    ESP_RETURN_ON_FALSE(message->info.status == ESP_ZB_ZCL_STATUS_SUCCESS, ESP_ERR_INVALID_ARG, TAG, "Received message: error status(%d)",
                        message->info.status);
    ESP_LOGI(TAG, "Received message: endpoint(%d), cluster(0x%x), attribute(0x%x), data size(%d)", message->info.dst_endpoint, message->info.cluster,
             message->attribute.id, message->attribute.data.size);

    switch (message->info.dst_endpoint)
    {
    case ESP_ALARM_SWITCH_ENDPOINT:
        ESP_LOGI(TAG, "Alarm switch endpoint");
        break;

    case ESP_SERVICE_SWITCH_ENDPOINT:
        ESP_LOGI(TAG, "Service switch endpoint");
        break;

    case ESP_ALARM_LED_ENDPOINT:
        ESP_LOGI(TAG, "Alarm LED endpoint");

        // We only expect to receive a message on this endpoint if we have already
        // sent a message to turn on the alarm or service switch
        // Stop the timer that would have sent the message again
        esp_timer_stop(send_command_timeout_timer);

        switch (message->info.cluster)
        {
        case ESP_ZB_ZCL_CLUSTER_ID_ON_OFF:

            // Check attribute id
            switch (message->attribute.id)
            {
            case ESP_ZB_ZCL_ATTR_ON_OFF_ON_OFF_ID:

                if (message->attribute.data.type == ESP_ZB_ZCL_ATTR_TYPE_BOOL)
                {
                    bool value = *(bool *)message->attribute.data.value;
                    ESP_LOGI(TAG, "Alarm LED set to %s", value ? "On" : "Off");
                    set_alarm_led_level(value);

                    if (!value)
                    {
                        // If we have received notification to turn off the alarm LED, we can go back to deep sleep
                        zb_deep_sleep_start();
                    }
                }
                else
                {
                    ESP_LOGW(TAG, "Unsupported data type(0x%x)", message->attribute.data.type);
                }
                break;

            default:
                ESP_LOGW(TAG, "Unsupported attribute(0x%x)", message->attribute.id);
                break;
            } // End of switch (message->attribute.id)
            break;

        default:
            ESP_LOGW(TAG, "Unsupported cluster(0x%x)", message->info.cluster);
            break;
        } // End of switch (message->info.cluster)

        break;

    default:
        ESP_LOGW(TAG, "Unsupported endpoint(%d)", message->info.dst_endpoint);
        break;
    } // End of switch (message->info.dst_endpoint)
    return ret;
}

static esp_err_t zb_action_handler(esp_zb_core_action_callback_id_t callback_id, const void *message)
{
    esp_err_t ret = ESP_OK;

    ESP_LOGI(TAG, "Receive Zigbee action(0x%x) callback", callback_id);

    switch (callback_id)
    {
    case ESP_ZB_CORE_SET_ATTR_VALUE_CB_ID:
        ret = zb_attribute_handler((esp_zb_zcl_set_attr_value_message_t *)message);
        break;
    default:
        ESP_LOGW(TAG, "Receive Zigbee action(0x%x) callback is unknown", callback_id);
        break;
    }
    return ret;
}

static esp_zb_cluster_list_t *custom_on_off_switch_clusters_create()
{
    /* Create cluster for on_off_switches */
    esp_zb_cluster_list_t *on_off_switch_cluster_list = esp_zb_zcl_cluster_list_create();

    // An on_off_switch device has a couple of mandatory clusters
    // https://www.nxp.com/docs/en/user-guide/JN-UG-3076.pdf -> Chapter 2.3.1

    // Identify cluster -> Server and client side
    esp_zb_identify_cluster_cfg_t identify_cfg = {.identify_time = ESP_ZB_ZCL_IDENTIFY_IDENTIFY_TIME_DEFAULT_VALUE};
    esp_zb_attribute_list_t *identify_cluster = esp_zb_identify_cluster_create(&identify_cfg);
    // ESP_ERROR_CHECK(esp_zb_cluster_list_add_identify_cluster(on_off_switch_cluster_list, identify_cluster, ESP_ZB_ZCL_CLUSTER_CLIENT_ROLE));
    ESP_ERROR_CHECK(esp_zb_cluster_list_add_identify_cluster(on_off_switch_cluster_list, identify_cluster, ESP_ZB_ZCL_CLUSTER_SERVER_ROLE));

    // Basic cluster -> Server side
    esp_zb_basic_cluster_cfg_t basic_cfg = {
        .zcl_version = ESP_ZB_ZCL_BASIC_ZCL_VERSION_DEFAULT_VALUE,
        .power_source = ESP_ZB_ZCL_BASIC_POWER_SOURCE_DEFAULT_VALUE};
    esp_zb_attribute_list_t *basic_cluster = esp_zb_basic_cluster_create(&basic_cfg);
    ESP_ERROR_CHECK(esp_zb_basic_cluster_add_attr(basic_cluster, ESP_ZB_ZCL_ATTR_BASIC_MANUFACTURER_NAME_ID, "EKsGang"));
    ESP_ERROR_CHECK(esp_zb_basic_cluster_add_attr(basic_cluster, ESP_ZB_ZCL_ATTR_BASIC_MODEL_IDENTIFIER_ID, "Alarm"));
    ESP_ERROR_CHECK(esp_zb_cluster_list_add_basic_cluster(on_off_switch_cluster_list, basic_cluster, ESP_ZB_ZCL_CLUSTER_SERVER_ROLE));

    // On/Off switch cluster -> Client side
    esp_zb_on_off_cluster_cfg_t on_off_cfg = {.on_off = false};
    esp_zb_attribute_list_t *on_off_cluster = esp_zb_on_off_cluster_create(&on_off_cfg);
    ESP_ERROR_CHECK(esp_zb_cluster_list_add_on_off_cluster(on_off_switch_cluster_list, on_off_cluster, ESP_ZB_ZCL_CLUSTER_CLIENT_ROLE));

    return on_off_switch_cluster_list;
}

static esp_zb_cluster_list_t *custom_on_off_light_clusters_create() // esp_zb_on_off_light_cfg_t *on_off_light)
{
    /* Create cluster for on_off_light */
    esp_zb_cluster_list_t *on_off_light_cluster_list = esp_zb_zcl_cluster_list_create();

    // https://www.nxp.com/docs/en/user-guide/JN-UG-3076.pdf -> Chapter 2.4.1

    // Basic cluster -> Server side
    esp_zb_basic_cluster_cfg_t basic_cfg_light = {
        .zcl_version = ESP_ZB_ZCL_BASIC_ZCL_VERSION_DEFAULT_VALUE,
        .power_source = ESP_ZB_ZCL_BASIC_POWER_SOURCE_DEFAULT_VALUE};
    esp_zb_attribute_list_t *basic_cluster_light = esp_zb_basic_cluster_create(&basic_cfg_light);
    ESP_ERROR_CHECK(esp_zb_basic_cluster_add_attr(basic_cluster_light, ESP_ZB_ZCL_ATTR_BASIC_MANUFACTURER_NAME_ID, "EKsGang"));
    ESP_ERROR_CHECK(esp_zb_basic_cluster_add_attr(basic_cluster_light, ESP_ZB_ZCL_ATTR_BASIC_MODEL_IDENTIFIER_ID, "Alarm"));

    ESP_ERROR_CHECK(esp_zb_cluster_list_add_basic_cluster(on_off_light_cluster_list, basic_cluster_light, ESP_ZB_ZCL_CLUSTER_SERVER_ROLE));

    // Identify cluster -> Server side
    esp_zb_identify_cluster_cfg_t identify_cfg_light = {
        .identify_time = ESP_ZB_ZCL_IDENTIFY_IDENTIFY_TIME_DEFAULT_VALUE};
    esp_zb_attribute_list_t *identify_cluster_light = esp_zb_identify_cluster_create(&identify_cfg_light);
    ESP_ERROR_CHECK(esp_zb_cluster_list_add_identify_cluster(on_off_light_cluster_list, identify_cluster_light, ESP_ZB_ZCL_CLUSTER_SERVER_ROLE));

    // On/Off cluster -> Server side
    esp_zb_on_off_cluster_cfg_t on_off_cfg_light = {
        .on_off = false};
    esp_zb_attribute_list_t *on_off_cluster_light = esp_zb_on_off_cluster_create(&on_off_cfg_light);
    ESP_ERROR_CHECK(esp_zb_cluster_list_add_on_off_cluster(on_off_light_cluster_list, on_off_cluster_light, ESP_ZB_ZCL_CLUSTER_SERVER_ROLE));

    // Scenes cluster -> Server side
    esp_zb_scenes_cluster_cfg_t scenes_cfg = {
        .scenes_count = ESP_ZB_ZCL_SCENES_SCENE_COUNT_DEFAULT_VALUE,
        .current_scene = ESP_ZB_ZCL_SCENES_CURRENT_SCENE_DEFAULT_VALUE,
        .current_group = ESP_ZB_ZCL_SCENES_CURRENT_GROUP_DEFAULT_VALUE,
        .scene_valid = ESP_ZB_ZCL_SCENES_SCENE_VALID_DEFAULT_VALUE,
        .name_support = ESP_ZB_ZCL_SCENES_NAME_SUPPORT_DEFAULT_VALUE};
    esp_zb_attribute_list_t *scenes_cluster = esp_zb_scenes_cluster_create(&scenes_cfg);
    ESP_ERROR_CHECK(esp_zb_cluster_list_add_scenes_cluster(on_off_light_cluster_list, scenes_cluster, ESP_ZB_ZCL_CLUSTER_SERVER_ROLE));

    // Groups cluster -> Server side
    esp_zb_groups_cluster_cfg_t groups_cfg = {
        .groups_name_support_id = ESP_ZB_ZCL_GROUPS_NAME_SUPPORT_DEFAULT_VALUE};
    esp_zb_attribute_list_t *groups_cluster = esp_zb_groups_cluster_create(&groups_cfg);
    ESP_ERROR_CHECK(esp_zb_cluster_list_add_groups_cluster(on_off_light_cluster_list, groups_cluster, ESP_ZB_ZCL_CLUSTER_SERVER_ROLE));

    return on_off_light_cluster_list;
}

// static esp_zb_cluster_list_t *custom_smart_plug_clusters_create()
// {
// }

static void esp_zb_task(void *pvParameters)
{
    /* initialize Zigbee stack */
    esp_zb_cfg_t zb_nwk_cfg = ESP_ZB_ZED_CONFIG();
    esp_zb_init(&zb_nwk_cfg);

    /* Create endpoint list */
    esp_zb_ep_list_t *ep_list = esp_zb_ep_list_create();

    /* Create endpoint configurations */
    esp_zb_endpoint_config_t alarm_switch_ep_cfg = {
        .endpoint = ESP_ALARM_SWITCH_ENDPOINT,
        .app_profile_id = ESP_ZB_AF_HA_PROFILE_ID,
        .app_device_id = ESP_ZB_HA_ON_OFF_LIGHT_DEVICE_ID, // ESP_ZB_HA_ON_OFF_SWITCH_DEVICE_ID
        .app_device_version = 0};

    esp_zb_endpoint_config_t service_switch_ep_cfg = {
        .endpoint = ESP_SERVICE_SWITCH_ENDPOINT,
        .app_profile_id = ESP_ZB_AF_HA_PROFILE_ID,
        .app_device_id = ESP_ZB_HA_ON_OFF_LIGHT_DEVICE_ID, // ESP_ZB_HA_ON_OFF_SWITCH_DEVICE_ID
        .app_device_version = 0};

    esp_zb_endpoint_config_t alarm_led_ep_cfg = {
        .endpoint = ESP_ALARM_LED_ENDPOINT,
        .app_profile_id = ESP_ZB_AF_HA_PROFILE_ID,
        .app_device_id = ESP_ZB_HA_ON_OFF_LIGHT_DEVICE_ID,
        .app_device_version = 0};

    // ESP_ZB_HA_ON_OFF_SWITCH_DEVICE_ID
    // Switch devices expect the on/off cluster to be on the client side and expects to be binded to a light device
    // We don't want this functionality, we want the switch to be on the server side, yet still be able to change it
    // From this device
    // Another option is to use the smart switch device id, but this requires extra clusters,
    // which are not relevant for this project.

    /* Create on_off_light clusters*/
    esp_zb_cluster_list_t *alarm_switch_cluster_list = custom_on_off_light_clusters_create();
    esp_zb_cluster_list_t *service_switch_cluster_list = custom_on_off_light_clusters_create();
    esp_zb_cluster_list_t *alarm_led_cluster_list = custom_on_off_light_clusters_create();

    /* Add on_off_light clusters to endpoint list */
    ESP_ERROR_CHECK(esp_zb_ep_list_add_ep(ep_list, alarm_switch_cluster_list, alarm_switch_ep_cfg));
    ESP_ERROR_CHECK(esp_zb_ep_list_add_ep(ep_list, service_switch_cluster_list, service_switch_ep_cfg));
    ESP_ERROR_CHECK(esp_zb_ep_list_add_ep(ep_list, alarm_led_cluster_list, alarm_led_ep_cfg));

    /* Register device */
    ESP_ERROR_CHECK(esp_zb_device_register(ep_list));

    ESP_LOGI(TAG, "Endpoints created");
    esp_zb_core_action_handler_register(zb_action_handler);
    esp_zb_set_primary_network_channel_set(ESP_ZB_PRIMARY_CHANNEL_MASK);
    ESP_ERROR_CHECK(esp_zb_start(false));
    esp_zb_main_loop_iteration();
}

void zigbee_configuration(void)
{
    esp_zb_platform_config_t config = {
        .radio_config = ESP_ZB_DEFAULT_RADIO_CONFIG(),
        .host_config = ESP_ZB_DEFAULT_HOST_CONFIG(),
    };
    ESP_ERROR_CHECK(nvs_flash_init());
    ESP_ERROR_CHECK(esp_zb_platform_config(&config));

    // Lower zigbee antenna power
    esp_zb_set_tx_power(IEEE802154_TXPOWER_VALUE_MIN);

    int8_t tx_power = 0;
    esp_zb_get_tx_power(&tx_power);
    ESP_LOGI(TAG, "TX power: %d", tx_power);

    zb_deep_sleep_init();
    send_command_timeout_timer_init();

    // light_driver_init(LIGHT_DEFAULT_OFF);
    xTaskCreate(esp_zb_task, "Zigbee_main", 4096, NULL, 5, NULL);
}
