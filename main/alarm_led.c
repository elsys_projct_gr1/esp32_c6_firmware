#include "alarm_led.h"

static const char *TAG = "alarm_led";

void set_alarm_led_level(bool state)
{
    /* Set the GPIO level according to the state (LOW or HIGH)*/
    ESP_LOGI(TAG, "Setting alarm LED level to %d", state);
    gpio_set_level(ALARM_LED_GPIO, state);
}

void configure_alarm_led(void)
{
    /* Configure alarm LED */
    gpio_reset_pin(ALARM_LED_GPIO);

    gpio_config_t io_conf = {
        .pin_bit_mask = (1ULL << ALARM_LED_GPIO),
        .mode = GPIO_MODE_OUTPUT,
        .pull_up_en = GPIO_PULLUP_ENABLE,
        .pull_down_en = GPIO_PULLDOWN_DISABLE,
        .intr_type = GPIO_INTR_DISABLE};

    gpio_config(&io_conf);
}
