#include "sleep_configuration.h"

static const char *TAG = "sleep_configuration";

esp_err_t configure_deep_sleep_gpio_pins(void)
{
    // Alarm button
    ESP_ERROR_CHECK(esp_deep_sleep_enable_gpio_wakeup(ALARM_BUTTON_GPIO_MASK, ESP_GPIO_WAKEUP_GPIO_LOW));

    // Service buttons
    ESP_ERROR_CHECK(esp_deep_sleep_enable_gpio_wakeup(SERVICE_BUTTON_LEFT_GPIO_MASK, ESP_GPIO_WAKEUP_GPIO_LOW));
    ESP_ERROR_CHECK(esp_deep_sleep_enable_gpio_wakeup(SERVICE_BUTTON_RIGHT_GPIO_MASK, ESP_GPIO_WAKEUP_GPIO_LOW));

    return ESP_OK;
}

esp_err_t configure_deep_sleep_timer(uint64_t sleep_duration_us)
{
    esp_err_t ret;
    ret = esp_sleep_enable_timer_wakeup(sleep_duration_us);
    return ret;
}

void handle_wakeup_cause(void)
{
    const esp_sleep_source_t wakeup_source = esp_sleep_get_wakeup_cause();

    switch (wakeup_source)
    {
    case ESP_SLEEP_WAKEUP_GPIO:
        ESP_LOGI(TAG, "Wake up from GPIO");

        const uint64_t gpio_wakeup_cause = esp_sleep_get_gpio_wakeup_status();

        if (gpio_wakeup_cause & ALARM_BUTTON_GPIO_MASK)
        {
            ESP_LOGI(TAG, "Wake up from alarm button");
        }
        else if (gpio_wakeup_cause & (SERVICE_BUTTON_LEFT_GPIO_MASK | SERVICE_BUTTON_RIGHT_GPIO_MASK))
        {
            ESP_LOGI(TAG, "Wake up from service button");
        }

        // One of the alarm buttons was pressed, indicate with LED
        // set_alarm_led_level(true);
        break;

    case ESP_SLEEP_WAKEUP_TIMER:
        ESP_LOGI(TAG, "Wake up from timer");
        break;

    default:
        // This should never happen in practice
        ESP_LOGW(TAG, "Not a deep sleep reset\n");
        break;
    }
}