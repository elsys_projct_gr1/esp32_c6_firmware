#ifndef SLEEP_CONFIGURATION_H
#define SLEEP_CONFIGURATION_H

#include "esp_sleep.h"
#include "esp_log.h"
#include "alarm_buttons.h"

#define TIMER_WAKEUP_DURATION_US 20 * 1000000 // 20 seconds

esp_err_t configure_deep_sleep_gpio_pins(void);
esp_err_t configure_deep_sleep_timer(uint64_t sleep_duration_us);
void handle_wakeup_cause(void);

#endif // SLEEP_CONFIGURATION_H