
#ifndef ALARM_LED_H
#define ALARM_LED_H

#include <stdio.h>
#include "driver/gpio.h"
#include "esp_log.h"

#define ALARM_LED_GPIO GPIO_NUM_23

void configure_alarm_led(void);
void set_alarm_led_level(bool state);

#endif /* ALARM_LED_H */