# README

## Prerequisites
Clone this repository

If you are using VSCode, install the ESP-IDF extension and follow the installation guide.

Build :D

## Hardware
If using custom PCB, to be able to enter the correct download mode, add a 10k pull-up resistor to pin 10 (IO8).

To enter download mode to flash the device, hold the enable and boot button. Release the enable button while holding the boot button.
The blue LED should light up and stay on, the boot button can be released, you are now ready to flash the device.